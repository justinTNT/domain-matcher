module DomainCodingExercise.PropertyMatcher

open Types

let private splitOnSpaces (stringToSplit: string) : string array
    = stringToSplit.Split ' '

let internal reverseNameMatch (agencyProperty: Property) (dbProperty: Property) : bool =
    let
        reverseWords: (string -> string) =
            splitOnSpaces
            >> Array.rev
            >> String.concat " "

    (reverseWords agencyProperty.Name) = dbProperty.Name


let internal looseNameAddressMatch (agencyProperty: Property) (dbProperty: Property) : bool =
    let
        toUpperWithoutPunctuation (c: char) : char =
            if System.Char.IsPunctuation(c) then
                ' '
            else
                System.Char.ToUpper(c)
    let
        simplify : (string array -> string) =
            String.concat " "
            >> String.map toUpperWithoutPunctuation
            >> splitOnSpaces
            >> Array.where (String.length >> (<) 0)
            >> String.concat " "
    let
        looseMatch (first: string array) (second: string array) : bool =
            (first |> simplify) = (second |> simplify)

    looseMatch
        [|agencyProperty.Name; agencyProperty.Address|]
        [|dbProperty.Name; dbProperty.Address|]


let internal latLongNearby (agencyProperty: Property) (dbProperty: Property) : bool =
    let
        degreesPerMetre: float =
            1.0 / 111000.0
    let
        maxDistanceVariation: float =
            200.0 * degreesPerMetre
    let
        diffSquared (first: string) (second: string) : float =
            pown (float first - float second) 2

    agencyProperty.AgencyCode = dbProperty.AgencyCode
        && pown maxDistanceVariation 2 >=
            diffSquared agencyProperty.Latitude dbProperty.Latitude
            + diffSquared agencyProperty.Longitude dbProperty.Longitude


let isMatch (agencyProperty: Property) (dbProperty: Property) : bool =
    let comparator: (Property -> Property -> bool) =
        match agencyProperty.AgencyCode with
        | OTBRE ->
            looseNameAddressMatch
        | LRE ->
            latLongNearby
        | CRE ->
            reverseNameMatch
        | _ ->
            failwithf "invalid agency code"

    comparator agencyProperty dbProperty
