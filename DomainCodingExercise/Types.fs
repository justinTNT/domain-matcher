﻿module DomainCodingExercise.Types

type AgencyCode =
  | OTBRE
  | LRE
  | CRE
  | None

type Property =
  { Address: string
    AgencyCode: AgencyCode
    Name: string
    Latitude: string
    Longitude: string }
  static member Default =
    { Address = ""
      AgencyCode = None
      Name = ""
      Latitude = ""
      Longitude = "" }
  
