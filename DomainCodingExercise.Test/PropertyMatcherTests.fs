module DomainCodingExercise.Test

open Types
open NUnit.Framework

[<AutoOpen>]
module private Helper =
  let createTestOTBREPropertyForName (name, location) =
    { Property.Default with
        AgencyCode = OTBRE
        Name = name
        Address = location }

  let createTestLREPropertyForName (lat, long) =
    { Property.Default with
        AgencyCode = LRE
        Latitude = lat
        Longitude = long }

  let createTestCREPropertyForName (name) =
    { Property.Default with
        AgencyCode = CRE
        Name = name }

[<TestCase("*Super*-High! APARTMENTS (Sydney)", "32 Sir John-Young Crescent, Sydney, NSW.", "Super High Apartments, Sydney", "32 Sir John Young Crescent, Sydney NSW", true)>]
[<TestCase("*Super*-Low! APARTMENTS (Sydney)", "32 Sir John-Young Crescent, Sydney, NSW.", "Super High Apartments, Sydney", "32 Sir John Young Crescent, Sydney NSW", false)>]
[<TestCase("*Super*-High! APARTMENTS (Sydney)", "2 Sir John-Young Crescent, Sydney, NSW.", "Super High Apartments, Sydney", "32 Sir John Young Crescent, Sydney NSW", false)>]
let Test1(agencyProperty: string, agencyPropertyLocation: string, dbProperty: string, dbPropertyLocation: string, expected: bool) =
  let agencyProperty = createTestOTBREPropertyForName (agencyProperty, agencyPropertyLocation)
  let dbProperty = createTestOTBREPropertyForName (dbProperty, dbPropertyLocation)

  Assert.AreEqual(expected, PropertyMatcher.isMatch agencyProperty dbProperty) |> ignore

[<TestCase("-33.9", "151.2", "-33.8688", "151.2093", false)>]
[<TestCase("-35.8688", "152.2093", "-33.8688", "151.2093", false)>]
[<TestCase("-33.8688", "151.2093", "-33.8688", "151.2093", true)>]
let Test2(agencyLat: string, agencyLong: string, dbLat: string, dbLong: string, expected: bool) =
  let agencyProperty = createTestLREPropertyForName (agencyLat, agencyLong)
  let dbProperty = createTestLREPropertyForName (dbLat, dbLong)

  Assert.AreEqual(expected, PropertyMatcher.isMatch agencyProperty dbProperty) |> ignore

[<TestCase("Apartments Summit The", "The Summit Apartments", true)>]
[<TestCase("Apartment Summit The", "The Summit Apartments", false)>]
[<TestCase("The Summit Apartments", "The Summit Apartments", false)>]
let Test3(agencyProperty: string, dbProperty: string, expected: bool) =
  let agencyProperty = createTestCREPropertyForName (agencyProperty)
  let dbProperty = createTestCREPropertyForName (dbProperty)

  Assert.AreEqual(expected, PropertyMatcher.isMatch agencyProperty dbProperty) |> ignore

[<TestCase("-23.4500", "123.4000", "-23.4500", "123.4018", true)>]
[<TestCase("-23.4500", "123.4000", "-23.4500", "123.4019", false)>]
[<TestCase("-23.4518", "123.4000", "-23.4500", "123.4000", true)>]
[<TestCase("-23.4519", "123.4000", "-23.4500", "123.4000", false)>]
let TestMore(agencyLat: string, agencyLong: string, dbLat: string, dbLong: string, expected: bool) =
  let agencyProperty = createTestLREPropertyForName (agencyLat, agencyLong)
  let dbProperty = createTestLREPropertyForName (dbLat, dbLong)

  Assert.AreEqual(expected, PropertyMatcher.isMatch agencyProperty dbProperty) |> ignore

